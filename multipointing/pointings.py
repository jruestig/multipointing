import numpy as np
from numpy import cos, array
import nifty5 as ift


def relative_to_image_center(pointings, im_center=None):
    """Calculates pointings relative to image_center in flat-sky approximation,
    if not specified image_center gets calculated as the middle of the
    pointings which are farthest away."""
    if im_center is None:
        assert pointings.shape[1] == 2
        ra = (pointings.T[0].max() - pointings.T[0].min())/2.
        decl = (pointings.T[1].max() - pointings.T[1].min())/2.
        im_center = array([pointings.T[0].min() + ra,
                           pointings.T[1].min() + decl])
    res = pointings - im_center
    return array([res.T[0]*cos(pointings.T[1]), res.T[1]]).T


def get_pixel(domain, pointings, units=1):
    a = []
    for ii, maxp in enumerate(array(domain.distances)*array(domain.shape)/2.):
        x = np.linspace(-maxp, maxp, domain.shape[ii])
        la, lb = np.meshgrid(x, pointings.T[ii]/units)
        a.append(np.argmin(abs(la-lb), axis=1))
    return array(a).T


def field_coverage(domain, positions, counts=False, physical=False):
    """Used for plotting the sky coverage of pointings,
    takes an array of pixel positions and adds them to the domain grid"""
    coverage = np.zeros(domain.shape)
    if physical:
        positions = get_pixel(domain, positions)
    if counts is True:
        uniqu, counts = np.unique(positions, axis=0, return_counts=True)
        x, y = uniqu.T
        coverage[x, y] = counts
    else:
        x, y = positions.T
        coverage[x, y] += 1
    return ift.from_global_data(domain, coverage)


def PhaseShift(uv, pointing):
    assert len(uv.shape) == 2
    nrows = uv.shape[0]
    domain = ift.DomainTuple.make(ift.UnstructuredDomain(nrows))
    arr = uv.T[0] * pointing[0] + uv.T[1] * pointing[1]
    r = np.exp(2*np.pi*1j*arr)
    return ift.DiagonalOperator(ift.from_global_data(domain, r))


def rotation(alpha, ab):
    tmp = np.zeros_like(ab)
    a, b = np.cos(alpha), -np.sin(alpha)
    c, d = np.sin(alpha), np.cos(alpha)
    tmp[..., -2] = a * ab[..., -2] + b * ab[..., -1]
    tmp[..., -1] = c * ab[..., -2] + d * ab[..., -1]
    return tmp
