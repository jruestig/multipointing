import nifty5 as ift


class EnergySum(ift.EnergyOperator):
    def __init__(self, energies):
        assert type(energies) is list
        for ii in range(len(energies)-1):
            assert energies[ii].domain is energies[ii+1].domain

        self.energies = energies
        self._domain = ift.DomainTuple.make(energies[0].domain)
        print("Combining {} energies".format(len(energies)))

    def apply(self, x):
        self._check_input(x)
        res = self.energies[0](x)
        for energy in self.energies[1:]:
            res = res + energy(x)
        return res
