# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2019 Max-Planck-Society
#
# NIFTy is being developed at the Max-Planck-Institut fuer Astrophysik.
# Author: Philipp Arras, Martin Reinecke

import nifty5 as ift
import nifty_gridder


class GridderMaker(object):
    def __init__(self,
                 dirty_domain,
                 uvw,
                 freq,
                 flags,
                 eps=2e-13,
                 chbegin=-1,
                 chend=-1,
                 complex_mode=False,
                 units=1,
                 wrange=None):
        dirty_domain = ift.makeDomain(dirty_domain)
        if (len(dirty_domain) != 1
                or not isinstance(dirty_domain[0], ift.RGSpace)
                or not len(dirty_domain.shape) == 2):
            raise ValueError("need dirty_domain with exactly one 2D RGSpace")
        pixsize_x, pixsize_y = dirty_domain[0].distances
        if freq.ndim != 1:
            raise ValueError("freq must be a 1D array")
        bl = nifty_gridder.Baselines(coord=uvw*units, freq=freq)
        nxdirty, nydirty = dirty_domain.shape
        gconf = nifty_gridder.GridderConfig(
            nxdirty=nxdirty,
            nydirty=nydirty,
            epsilon=eps,
            pixsize_x=pixsize_x,
            pixsize_y=pixsize_y)
        nu = gconf.Nu()
        nv = gconf.Nv()
        dct = {
            'baselines': bl,
            'gconf': gconf,
            'flags': flags,
            'chbegin': int(chbegin),
            'chend': int(chend)
        }
        if wrange is not None:
            assert len(wrange) == 2
            self._idx = nifty_gridder.getIndices(
                **dct, wmin=wrange[0], wmax=wrange[1])
            if len(self._idx) == 0:
                raise RuntimeError('Empty w plane. This is bug.')
        else:
            self._idx = nifty_gridder.getIndices(**dct)
        self._bl = bl
        self.n_sampling_points = len(self._idx)

        grid_domain = ift.RGSpace([nu, nv], distances=[1, 1], harmonic=False)

        cm = bool(complex_mode)
        self._rest = _RestOperator(dirty_domain, grid_domain, gconf, cm)
        self._gridder = RadioGridder(grid_domain, bl, gconf, self._idx, cm)

    def getGridder(self):
        return self._gridder

    def getRest(self):
        return self._rest

    def getFull(self):
        return self.getRest() @ self._gridder

    def ms2vis(self, x):
        return self._bl.ms2vis(x, self._idx)


class _RestOperator(ift.LinearOperator):
    def __init__(self, dirty_domain, grid_domain, gconf, complex_mode=False):
        self._domain = ift.makeDomain(grid_domain)
        self._target = ift.makeDomain(dirty_domain)
        self._gconf = gconf
        self._capability = self.TIMES | self.ADJOINT_TIMES
        self._complex_mode = bool(complex_mode)

    def apply(self, x, mode):
        self._check_input(x, mode)
        res = x.to_global_data()
        if mode == self.TIMES:
            f = self._gconf.grid2dirty
            if self._complex_mode:
                f = self._gconf.grid2dirty_c
        else:
            f = self._gconf.dirty2grid
            if self._complex_mode:
                f = self._gconf.dirty2grid_c
        return ift.from_global_data(self._tgt(mode), f(res))


class RadioGridder(ift.LinearOperator):
    def __init__(self, grid_domain, bl, gconf, idx, complex_mode=False):
        self._domain = ift.DomainTuple.make(ift.UnstructuredDomain((len(idx))))
        self._target = ift.DomainTuple.make(grid_domain)
        self._bl = bl
        self._gconf = gconf
        self._idx = idx
        self._capability = self.TIMES | self.ADJOINT_TIMES
        self._complex_mode = bool(complex_mode)

    def apply(self, x, mode):
        self._check_input(x, mode)
        if mode == self.TIMES:
            f = nifty_gridder.vis2grid
            if self._complex_mode:
                f = nifty_gridder.vis2grid_c
        else:
            f = nifty_gridder.grid2vis
            if self._complex_mode:
                f = nifty_gridder.grid2vis_c
        res = f(self._bl, self._gconf, self._idx, x.to_global_data())
        return ift.from_global_data(self._tgt(mode), res)
