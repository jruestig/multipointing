import nifty5 as ift
import numpy as np
from scipy import ndimage

import os
from os.path import join, exists
import pickle

import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from matplotlib.gridspec import GridSpec
from matplotlib import cm
from matplotlib.colors import ListedColormap, LinearSegmentedColormap
from .colormaps.test import red_cm, violet_cm

from astropy.io import fits
from .pointings import get_pixel


viridis = cm.get_cmap('viridis')
jy = r"$\mathrm{Jy} / \mathrm{arcsec}^2$"
plt.rcParams.update({'font.size': 22})



def _sky_plotter(dom, sky, ax, units=1, norm=None, title='', zlabel="", 
                 cmap=viridis, maximum=None):
    norm = {} if norm is None else {'norm': norm}
    nx, ny = dom.shape
    dx, dy = np.array(dom.distances)*units
    if maximum is None:
        im = ax.imshow(
            sky.to_global_data().T,
            origin='lower',
            extent=[(nx*dx + 0.5*dx)/2, -(nx*dx)/2, -(ny*dy + 0.5*dy)/2,
                    (ny*dy)/2],
            cmap=cmap,
            **norm)
    else:
        im = ax.imshow(
            sky.to_global_data().T,
            origin='lower',
            extent=[(nx*dx + 0.5*dx)/2, -(nx*dx)/2, -(ny*dy + 0.5*dy)/2,
                    (ny*dy)/2],
            clim=(0., maximum),
            cmap=cmap,
            **norm)
    ax.set_title(title)
    ax.set_xlabel('RA [arcsec]')
    ax.set_ylabel('DECL [arcsec]')
    from mpl_toolkits.axes_grid1 import make_axes_locatable
    divider = make_axes_locatable(ax)
    cax = divider.append_axes('right', size='5%', pad=0.05)
    cb = plt.colorbar(im, cax=cax)
    cb.set_label("{}".format(zlabel))
    # plt.colorbar(im)


def _harmonic_plotter(dom, sky, ax, units=1, norm=None, title='', zlabel="",
                      maximum=None):
    norm = {} if norm is None else {'norm': norm}
    nx, ny = dom.shape
    dx, dy = np.array(dom.distances)/(units)
    im = ax.imshow(
        sky.to_global_data(),
        origin='lower',
        extent=[(nx*dx + 0.5*dx)/2, -(nx*dx)/2, -(ny*dy + 0.5*dy)/2,
                (ny*dy)/2],
        **norm)
    ax.set_title(title)
    ax.set_xlabel('1/RA [1/arcsec]')
    ax.set_ylabel('1/DECL [1/arcsec]')
    from mpl_toolkits.axes_grid1 import make_axes_locatable
    divider = make_axes_locatable(ax)
    cax = divider.append_axes('right', size='5%', pad=0.05)
    cb = plt.colorbar(im, cax=cax)
    cb.set_label("{}".format(zlabel))
    # plt.colorbar(im)


def _to_hspace(field):
    s_space = field.domain[0]
    h_space = s_space.get_default_codomain()
    ft = ift.HartleyOperator(h_space, s_space)
    return ft.inverse(field)


class Plotter:
    def __init__(self, path, diffuse, amplitude, RNd, dof=0, units=1,
                 mocksky=None):
        self.spatial_space = diffuse.target[0]
        self.harmonic_space = ift.get_default_codomain(self.spatial_space)
        self.diffuse = diffuse
        if not exists(path):
            os.makedirs(path)
        self.path = path
        self.mocksky = mocksky
        self.amplitude = amplitude
        self.RNd = RNd
        self.dof_size = 0.5 * dof
        for _, _, d in RNd:
            self.dof_size += 0.5 * d.domain.size
        self.units = units
        self.pspec_limits = None

    def plot(self, KL, mockpos=None, pspec_limits=None, suffix="posterior"):
        mean = KL.position
        samples = KL.samples
        sc = ift.StatCalculator()
        tmpsky = self.diffuse(mean)
        for sample in samples:
            samplesky = self.diffuse.force(sample.unite(mean))
            sc.add(samplesky)
        qu = ift.sqrt(sc.var)/sc.mean  # Squared Error/Variance
        maximum = sc.mean.val.max()

        residual = ift.full(self.diffuse.target, 0.)
        for R, N, d in self.RNd:
            residual = residual + R.adjoint(N.inverse(d-R(tmpsky)))

        if pspec_limits is not None:
            self.pspec_limits = pspec_limits

        fig = plt.figure()
        # FIXME: This could be represented with a for loop
        if mockpos is not None:
            fig.set_size_inches(24, 28)
            gs = GridSpec(6, 6)
            ax_syn = fig.add_subplot(gs[0:2, 0:2])
            ax_mean = fig.add_subplot(gs[0:2, 2:4])
            ax_var = fig.add_subplot(gs[0:2, 4:6])
            ax_res = fig.add_subplot(gs[2:4, 0:2])
            ax_qu = fig.add_subplot(gs[2:4, 2:4])
            ax_sqerr = fig.add_subplot(gs[2:4, 4:6])
            ax_amp = fig.add_subplot(gs[4:6, 0:6])

            mocksky = self.diffuse(mockpos)
            maximum = mocksky.to_global_data().max()
            squarederror = (sc.mean - mocksky)**2

            _sky_plotter(self.spatial_space, mocksky, ax_syn, units=self.units,
                         title="Synthetic Sky", zlabel=jy,
                         maximum=maximum)
            _sky_plotter(self.spatial_space, squarederror, ax_sqerr,
                         units=self.units, title="Squared Error")
            self.amplitude_analysis(ax_amp, mean, samples, mockpos=mockpos)
        else:
            fig.set_size_inches(25, 16)
            gs = GridSpec(4, 6)
            ax_mean = fig.add_subplot(gs[0:2, 0:2])
            ax_var = fig.add_subplot(gs[0:2, 2:4])
            ax_qu = fig.add_subplot(gs[0:2, 4:6])
            ax_res = fig.add_subplot(gs[2:4, 0:2])
            ax_amp = fig.add_subplot(gs[2:4, 2:6])
            self.amplitude_analysis(ax_amp, mean, samples)

        _sky_plotter(self.spatial_space, sc.mean, ax_mean, units=self.units,
                     title="Sky Mean", zlabel=jy, maximum=maximum)
        _sky_plotter(self.spatial_space, ift.sqrt(sc.var), ax_var,
                     units=self.units, title="Sky Standard Deviation")
        _sky_plotter(self.spatial_space, qu, ax_qu, units=self.units,
                     title="Sky Variance / Mean")
        _sky_plotter(self.spatial_space, residual, ax_res, units=self.units,
                     title="Residual")
        fig.suptitle(u'KL.value: {} | dof+data: {} | KL/dof: {}'.format(
            KL.value, self.dof_size, KL.value/self.dof_size),
            fontsize=24, fontweight='bold')

        fig.tight_layout()
        plt.savefig(join(self.path, '{}.png'.format(suffix)))
        plt.close()

    def plot_fits(self, KL, suffix='posterior'):
        mean = KL.position
        samples = KL.samples
        sc = ift.StatCalculator()
        for sample in samples:
            samplesky = self.diffuse.force(sample.unite(mean))
            sc.add(samplesky)
        hdu = fits.PrimaryHDU(sc.mean.to_global_data())
        hdulist = fits.HDUList([hdu])
        hdulist.writeto(join(self.path, '{}.fits'.format(suffix)),
                        overwrite=True)

    def amplitude_analysis(self, ax, mean, samples, mockpos=None):
        amplitude = self.amplitude.force(mean)
        dom = self.diffuse.target[0].get_default_codomain()
        vol = dom.scalar_dvol**(-0.5)
        pspec = (vol*amplitude)**2
        logdiffuse = ift.log(self.diffuse)
        logsky_h = _to_hspace(logdiffuse.force(mean))

        k_lengths = (
            ift.power_analyze(logsky_h).domain[0].k_lengths/(self.units))
        # ax = plt.subplot(111)

        if mockpos is not None:
            true_ampl = self.amplitude.force(mockpos)
            true_pspec = (vol*true_ampl)**2
            ax.plot(
                k_lengths,
                # 'xkcd:light orange',
                true_pspec.to_global_data(),
                label='True power spectrum')

        ax.plot(
            k_lengths,
            pspec.to_global_data(),
            label='Power spectrum mean')

        sc_pspec_realization = ift.StatCalculator()
        label = 'Samples'
        for ii, samp in enumerate(samples):
            sample_pos = samp.unite(mean)
            logsky_h = _to_hspace(logdiffuse.force(sample_pos))
            realizsation = ift.power_analyze(logsky_h)
            sc_pspec_realization.add(realizsation)
            ax.plot(
                k_lengths,
                realizsation.to_global_data(),
                'maroon',
                alpha=.1,
                label=label)
            label = ''
        ax.plot(
            k_lengths,
            sc_pspec_realization.mean.to_global_data(),
            'maroon',
            label='Power spectrum sample mean',
            alpha=.5)
        ax.loglog()
        ax.legend()
        if self.pspec_limits is not None:
            ax.set_ylim(self.pspec_limits)
        else:
            self.pspec_limits = ax.get_ylim()
        ax.set_xlabel('Spatial frequency [1/arcsec]')
        ax.set_ylabel('Power')

    def save_pickle(self, position, file_name, path=None):
        import pickle
        if path is None:
            path = self.path
        else:
            path = join(self.path, path)
        with open(join(path, file_name + '.pickle'), 'wb') as f:
            pickle.dump(position, f, pickle.HIGHEST_PROTOCOL)

    def show(self, field):
        fig = plt.figure()
        fig.set_size_inches(16, 16)
        ax = fig.add_subplot(2, 1, 1)
        _sky_plotter(self.spatial_space, field, ax, units=self.units,
                     title="")
        ax = fig.add_subplot(2, 1, 2)
        _sky_plotter(self.spatial_space, field, ax, units=self.units,
                     norm=LogNorm(), title="")
        fig.tight_layout()
        plt.show()
        plt.close()

    def plot_field(self, field, suffix="field"):
        fig = plt.figure()
        fig.set_size_inches(16, 16)
        ax = fig.add_subplot(2, 1, 1)
        _sky_plotter(self.spatial_space, field, ax, units=self.units,
                     title="")
        ax = fig.add_subplot(2, 1, 2)
        _sky_plotter(self.spatial_space, field, ax, units=self.units,
                     norm=LogNorm(), title="")
        fig.tight_layout()
        plt.savefig(join(self.path, '{}.png'.format(suffix)))
        plt.close()

    def set_up(self, primary_beam, sd_coverage, sd_adjoint,
               uv_coverage, inter_pointings, inter_adjoint):
        fig = plt.figure()
        fig.set_size_inches(24, 28)
        gs = GridSpec(6, 6)
        ax1 = fig.add_subplot(gs[0:2, 0:2])
        ax2 = fig.add_subplot(gs[0:2, 2:4])
        ax3 = fig.add_subplot(gs[0:2, 4:6])
        ax4 = fig.add_subplot(gs[2:4, 0:2])
        ax5 = fig.add_subplot(gs[2:4, 2:4])
        ax6 = fig.add_subplot(gs[2:4, 4:6])
        ax7 = fig.add_subplot(gs[4:7, 0:3])
        ax8 = fig.add_subplot(gs[4:7, 3:6])
        _sky_plotter(self.spatial_space, self.mocksky, ax1, units=self.units,
                     title="Mocksky", zlabel=jy)
        _sky_plotter(self.spatial_space, self.mocksky, ax2, units=self.units,
                     norm=LogNorm(), title="Logarithm Mocksky")
        _sky_plotter(self.spatial_space, primary_beam, ax3, units=self.units,
                     title="Primary Beam Pattern")
        _harmonic_plotter(self.harmonic_space, uv_coverage, ax4,
                          units=self.units, title="Fourier Coverage")
        _sky_plotter(self.spatial_space, sd_coverage, ax5, units=self.units,
                     title="Sd Pointings")
        _sky_plotter(self.spatial_space, inter_pointings, ax6,
                     units=self.units, title="Interferometer Pointings")
        _sky_plotter(self.spatial_space, sd_adjoint, ax7, units=self.units,
                     title="Sd Informationsource")
        _sky_plotter(self.spatial_space, inter_adjoint, ax8, units=self.units,
                     title="Inter Informationsource")
        fig.tight_layout()
        plt.savefig(join(self.path, 'set_up.png'))
        plt.close()

    def information(self, mode, name='information_source'):
        combined = ift.full(self.diffuse.target, 0.)
        for R, N, d in self.RNd:
            combined = combined + R.adjoint(N.inverse(d))

        fig = plt.figure()
        if mode is "sd" or mode is "inter":
            fig.set_size_inches(16, 16)
            ax = fig.add_subplot(1, 1, 1)
            _sky_plotter(self.spatial_space, combined, ax, title=mode,
                         units=self.units)
        else:
            fig.set_size_inches(24, 8)
            sd_info = self.RNd[0][0].adjoint(
                self.RNd[0][1].inverse(self.RNd[0][2]))
            ax = fig.add_subplot(1, 3, 1)
            _sky_plotter(self.spatial_space, sd_info, ax, title="sd",
                         units=self.units)
            ax = fig.add_subplot(1, 3, 2)
            _sky_plotter(self.spatial_space, combined-sd_info, ax,
                         title="inter", units=self.units)
            ax = fig.add_subplot(1, 3, 3)
            _sky_plotter(self.spatial_space, combined, ax, title="combined",
                         units=self.units)

        fig.tight_layout()
        plt.savefig(join(self.path, '{}.png'.format(name)))
        plt.close()


class PosteriorPlotter():
    """ Container Class for posterior plotting """
    def __init__(self):
        self.arcmintorad = lambda x: np.pi/(60 * 180) * x #  1' × π/(60 × 180) 
        self.radtoarcsec = lambda x: (60 * 60 * 180)/np.pi * x #  1' × π/(60 × 180) 
        self.degtorad = lambda x: np.pi/(180) * x

    def get_rectangular(self, domain, point1, point2, units=1):
        '''Returns pixel positions of the rectangle spanned by point1, point2
        xmin, xmax, ymin, ymax'''
        assert len(domain[0].shape) is 2
        p = []
        for point in [point1, point2]:
            p.append(get_pixel(domain[0], point, units=units)[0])
        p = np.array(p)
        return p[:, 0].min(), p[:, 0].max(), p[:, 1].min(), p[:, 1].max()

    def load_diffuse(self, cfg):
        Nx, Ny = cfg.sky['Nx'], cfg.sky['Ny']
        maxfreq, minfreq = cfg.sky['maxfreq'], cfg.sky['minfreq']
        fov = cfg.sky['fov']
        phscntr = cfg.sky['phscntr']
        rotang = cfg.sky['rotation']
        units = cfg.sky['units']

        s_space = ift.RGSpace((Nx, Ny), distances=(fov/max([Nx, Ny]),)*2)
        h_space = s_space.get_default_codomain()
        ht = ift.HarmonicTransformOperator(h_space, s_space)
        power_space = ift.PowerSpace(h_space)

        sky_amp_conf = cfg.prior
        sky_amp_conf['keys'] = ['tau', 'phi']
        sky_amp_conf['target'] = power_space
        sky_amp = ift.SLAmplitude(**sky_amp_conf)
        A = ift.PowerDistributor(h_space, sky_amp.target[0]) @ sky_amp
        corr = ift.CorrelatedField(s_space, sky_amp)
        try:
            scale = ift.ScalingOperator(cfg.sky['scale'], corr.target)
        except TypeError:
            scale = ift.ScalingOperator(1., corr.target)
        return scale @ (corr.clip(-200, 200)).exp()

    def load_resolve(self, cfg, pckl):
        diffuse = self.load_diffuse(cfg)
        position = pickle.load(open(pckl, 'rb'))

        res_reconstruction = diffuse(position)
        res_reconstruction = ndimage.rotate(
                res_reconstruction.to_global_data(), -90, reshape=True)
        res_domain = ift.RGSpace(
                np.flip(np.array(diffuse.target[0].shape)), 
                self.arcmintorad(np.flip(np.array(diffuse.target[0].distances))))
        return ift.from_global_data(res_domain, res_reconstruction)

    def load_casa(self, cfg, fits_file, freq_dist=0):
        data, header = fits.getdata(fits_file, header=True)

        # get same freq image
        crval3 = header['CRVAL3']
        cdelt3 = header['CDELT3']
        naxis3 = header['NAXIS3']
        freq_ax = crval3 + (np.arange(naxis3) * cdelt3)
        fpix = ((freq_ax > cfg.sky['minfreq'] + freq_dist) * (freq_ax < cfg.sky['maxfreq'] + freq_dist)).argmax()
        casa_reconstruction = data[0][fpix]
        casa_reconstruction[np.isnan(casa_reconstruction)] = 0

        # rotate in the same way & shift to same center & coordinate system 
        img = ndimage.rotate(casa_reconstruction, np.degrees(cfg.sky['rotation']), reshape=False)
        center = np.array([self.degtorad(i) for i in [header['CRVAL1']-360, header['CRVAL2']]])
        nax1, delt1 = header['NAXIS1'], self.degtorad(-header['CDELT1'])
        nax2, delt2 = header['NAXIS2'], self.degtorad(header['CDELT2'])
        shift_center = cfg.sky['phscntr'] - center
        shift = np.array([-shift_center[0]/delt1, shift_center[1]/delt2])
        casa_reconstruction = ndimage.shift(img, shift)
        casa_domain = ift.RGSpace([nax1, nax2], distances=(delt1, delt2))
        casa_reconstruction = ift.from_global_data(casa_domain, casa_reconstruction)
        return casa_reconstruction, header
        


class PriorPlotter():
    def __init__(self, path, diffuse, units=1):
        self.spatial_space = diffuse.target[0]
        self.harmonic_space = ift.get_default_codomain(self.spatial_space)
        self.diffuse = diffuse
        if not exists(path):
            os.makedirs(path)
        self.path = path
        self.units = units

    def plot_prior(self, times, **dict):
        stats = [["Num", "sky.mean", "sky.var", "sky.min", "sky.max"]]

        fig = plt.figure()
        nx, ny = self.calc_fig_size(times)
        fig.set_size_inches(8*nx, 8*ny)
        for ii in range(times):
            rnd = ift.from_random("normal", self.diffuse.domain)
            y = self.diffuse(rnd)
            ax = fig.add_subplot(nx, ny, ii+1)
            _sky_plotter(self.spatial_space, y, ax, units=self.units)
            stats.append([ii, y.mean(), y.var(), y.val.min(), y.val.max()])
            print(ii, ":", y.mean(), y.var(), y.val.min(), y.val.max())

        path = join(self.path, "prior_samples")
        if not exists(path):
            os.makedirs(path)

        if exists(join(path, 'prior_samples.png')):
            from shutil import copyfile
            copyfile(join(path, 'prior_samples.png'),
                     join(path, 'prior_samples_0.png'))

        fig.tight_layout()
        plt.savefig(join(path, 'prior_samples.png'))
        plt.close()

        import csv
        w = csv.writer(open(join(path, "prior_stats.csv"), "w"))
        for line in stats:
            w.writerow(line)

    @staticmethod
    def calc_fig_size(integer):
        size = int(np.ceil(np.sqrt(integer)))
        nx, ny = (size, size)
        if size * (size - 1) >= integer:
            ny = size - 1
        return nx, ny
