import nifty5 as ift
import numpy as np
from .pointings import get_pixel


class FieldReducer(ift.LinearOperator):
    def __init__(self, domain, xmin, xmax, ymin, ymax):
        assert len(domain.shape) == 2
        self._capability = self.TIMES | self.ADJOINT_TIMES
        self._domain = ift.DomainTuple.make(domain)
        self._target = ift.DomainTuple.make(
            ift.RGSpace((xmax - xmin, ymax - ymin),
                        distances=domain[0].distances))

        self.xmin = xmin
        self.xmax = xmax
        self.ymin = ymin
        self.ymax = ymax

    def apply(self, field, mode):
        self._check_input(field, mode)
        field = field.to_global_data()
        if mode == self.TIMES:
            subfield = field[self.xmin:self.xmax, self.ymin:self.ymax]
            return ift.from_local_data(self._target, subfield)
        else:
            refilled = np.zeros(self._domain.shape)
            refilled[self.xmin:self.xmax, self.ymin:self.ymax] = field
            return ift.from_local_data(self._domain, refilled)


def get_pixel_patch(domain, pointing, size, units=1):
    assert len(size) == 2
    center = get_pixel(domain, pointing/units)[0]
    xmin, xmax = center[0] + np.array([-1, 1]) * (size[0] // 2)
    ymin, ymax = center[1] + np.array([-1, 1]) * (size[1] // 2)
    assert xmin > 0 and xmax < domain.shape[0]
    assert ymin > 0 and ymax < domain.shape[1]
    return xmin, xmax, ymin, ymax


def patch_size(domain, dish, factor=1.5, units=1):
    null = 1.22 * dish[0] ** -1
    first_null = null/domain.distances[0] / units
    return int(np.ceil(factor * 2 * first_null))
