from configparser import ConfigParser
from os.path import join, exists
from os import makedirs
import numpy as np


class ConfigurationParser():
    def __init__(self, config_file, postplt=False):
        cfg = ConfigParser()
        cfg.read(config_file)

        general_info = cfg['General']
        self.output = general_info['output']
        self.sky = self.get_sky_information(cfg)
        self.sd = self.get_sd_info(cfg)
        self.inter = self.get_inter_info(cfg)
        self.minimization = self.get_minimization(cfg)
        self.prior = self.get_prior(cfg)

        if (self.sd is None) and (self.inter is None):
            raise RuntimeError('No dataset loaded')
        elif self.inter is None:
            self.mode = 'sd'
        elif self.sd is None:
            self.mode = 'inter'
        else:
            self.mode = 'combined'

        if not postplt:
            if not exists(self.output):
                makedirs(self.output)
            f = open(join(self.output, 'config.cfg'), 'w')
            cfg.write(f)
            f.close()

    def get_sky_information(self, cfg):
        sky_cfg = cfg['Sky']
        phscntr = sky_cfg['phscntr'].split(', ')
        return {
            'Nx': sky_cfg.getint('Nx'),
            'Ny': sky_cfg.getint('Ny'),
            'fov': sky_cfg.getfloat('fov'),
            'units': sky_cfg.getfloat('units'),
            'maxfreq': sky_cfg.getfloat('maxfreq'),
            'minfreq': sky_cfg.getfloat('minfreq'),
            'phscntr': np.array([float(phscntr[0]), float(phscntr[1])]),
            'rotation': sky_cfg.getfloat('rotation'),
            'scale': sky_cfg.getfloat('scale')
        }

    def get_sd_info(self, cfg):
        try:
            sd_cfg = cfg['Single Dish']
            return {
                'sd_base': sd_cfg['sd_base'],
                'dish': np.array(
                    [sd_cfg.getfloat('D'), sd_cfg.getfloat('small_d')]),
                'sd_names': sd_cfg['sd_names'].splitlines()
            }
        except KeyError:
            print('No single dish data loaded')
            return None

    def get_inter_info(self, cfg):
        try:
            inter_info = cfg['Interferometer']
            inter_list = []
            for ii in range(inter_info.getint('data_sets')):  # loop through all interferometer datasets
                lines = inter_info['info{}'.format(ii)].splitlines()
                print(lines[0])
                if len(lines) == 6: lines.append(None)
                inter_list.append({
                    'inter_base': lines[0],
                    'field_ids': [int(fid) for fid in lines[1].split(', ')],
                    'dish': np.array([float(lines[2]), float(lines[3])]),
                    'band_id': int(lines[4]),
                    'pb_factor': float(lines[5]),
                    'psize': self.convert(lines[6], typ='int')
                })
            return inter_list
        except KeyError:
            print('No interferometer data loaded')
            return None

    def get_minimization(self, cfg):
        mini_cfg = cfg['Minimization']
        return {
            'maxsteps': mini_cfg.getint('maxsteps'),
            'tolerance': mini_cfg.getfloat('tolerance'),
            'Nsamples': mini_cfg.getint('Nsamples'),
            'cg_sampling': mini_cfg.getint('cg_sampling'),
            'N_iterations': mini_cfg.getint('N_iterations'),
        }

    def get_prior(self, cfg):
        prior_cfg = cfg['Prior']
        return {
            'n_pix': prior_cfg.getint('Npix'),
            'a': prior_cfg.getfloat('a'),
            'k0': prior_cfg.getfloat('k0'),
            'sm': prior_cfg.getfloat('sm'),
            'sv': prior_cfg.getfloat('sv'),
            'im': prior_cfg.getfloat('im'),
            'iv': prior_cfg.getfloat('iv'),
        }

    def convert(self, value, typ=None):
        if value is None: return value
        if typ is 'int': return int(value)
        if typ is 'float': return float(value)
