import scipy.special as sc
import numpy as np
import nifty5 as ift


def normalized_power_pattern(u, D, d):
    """
    D = Dish diameter / lambda
    d = blockage diameter / lambda
    u = sin(theta) = angle from pointing on sky (theta in rad)
    """
    b = d/D
    x = np.pi*D*u
    mask = (x == 0.)
    x[mask] = 1.
    sol = 2/(x*(1 - b**2))*(sc.jn(1, x) - b*sc.jn(1, x*b))
    sol[mask] = 1.
    return sol


def get_coordinate_field(domain):
    """zero centered coordinate field"""
    assert len(domain.shape) is 2
    xmax, ymax = np.array(domain.shape) * np.array(domain.distances) / 2.
    x = np.linspace(-xmax, xmax, num=domain.shape[0])
    y = np.linspace(-ymax, ymax, num=domain.shape[1])
    return np.meshgrid(x, y, indexing="ij")


def get_primary_beam(domain, relative_pos, dish, zero_pad=False,
                     units=1):
    if zero_pad:
        zero = ift.FieldZeroPadder(domain, np.array(domain.shape)*2)
        domain = zero.target[0]
    l, m = get_coordinate_field(domain)
    u = np.sqrt((l - relative_pos[0])**2 + (m - relative_pos[1])**2) * units
    return ift.from_global_data(
        domain, normalized_power_pattern(u, dish[0], dish[1])**2)
