import nifty5 as ift
import numpy as np
from scipy.sparse import csr_matrix
from scipy.sparse.linalg import aslinearoperator


def SingleDish(domain, xy, pb_field):
    assert (np.array(domain.shape)*2 == np.array(pb_field.shape)).all(), "Pb must be zero padded"
    smooth = HarmonicSmoothingKernelField(domain, pb_field)
    return SparseSelect(xy, domain) @ smooth


def HarmonicSmoothingKernelField(domain, field):
    assert (np.array(domain.shape)*2 == np.array(field.shape)).all(), "Pb must be zero padded"
    zero = ift.FieldZeroPadder(domain, np.array(domain.shape)*2)
    domain = zero.target[0]

    shifted = ift.Field.from_global_data(
        domain, np.fft.fftshift(field.to_global_data()))

    FFT = ift.HartleyOperator(
        domain=domain, target=domain.get_default_codomain())
    kernel = ift.makeOp(FFT(shifted*1./domain.dvol))

    Oper = zero.adjoint @ FFT.inverse @ kernel @ FFT @ zero
    return Oper


class SparseSelect(ift.LinearOperator):
    def __init__(self, xy, domain):
        assert len(xy.shape) == 2
        assert xy.shape[1] == 2
        self._domain = ift.DomainTuple.make(domain)
        self._target = ift.DomainTuple.make(
            ift.UnstructuredDomain(xy.shape[0]))
        self._capability = self.TIMES | self.ADJOINT_TIMES

        # Matrix Initialization
        w = np.ones_like(xy.T[0])
        col = xy.T[0]*domain.shape[1] + xy.T[1]
        row = np.arange(len(xy.T[0]))
        shape = (len(xy.T[0]), domain.shape[0]*domain.shape[1])
        Matrix = csr_matrix((w, (row, col)), shape=shape)
        self._smat = aslinearoperator(Matrix)

    def apply(self, x, mode):
        self._check_input(x, mode)
        if mode == self.TIMES:
            result = self._smat.matvec(x.to_global_data().flatten())
            return ift.Field.from_global_data(self._target, result)
        field = self._smat.rmatvec(x.to_global_data()).reshape(
            self._domain.shape)
        return ift.Field.from_global_data(self._domain, field)
