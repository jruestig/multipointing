# multipointing
Library for radio data multi instrument reconstructions


Requirements:
-------------------------------------------------------------------------------
- Python 3 (3.5.x or later)
- SciPy
- pypocketfft
- NIFTy5 (checkout 2e63dd00d3bf7775eca6f87188ba53e62897f587)
- nifty_gridder (for radio interferometry responses)
- matplotlib (for field plotting)
- astropy
- nifty gridder
- h5py
- casacore


Support for the radio interferometry gridder is added via:

    pip3 install --user git+https://gitlab.mpcdf.mpg.de/ift/nifty_gridder.git


Pypocketfft is added via:

    pip3 install --user git+https://gitlab.mpcdf.mpg.de/mtr/pypocketfft



Realdata:
-------------------------------------------------------------------------------
1)  Data extraction
- Download ALMA data,  http://almascience.nrao.edu/aq/  under project code 2013.1.01161.S   

- Extract interferometer data via ms2hdf5.py


        python3 ms2hdf5.py uid___A002_X9a9a13_X13cd.ms.split.cal/

- Extract single dish data via extract_data_sd.py, e.g.


        python3 extract_data_sd.py uid___A002_Xb12f3b_Xb716.PM01.ms 0 0 0 


2)  Reconstructions
you will need to adjust the file path in the inits

- Compact reconstuction
        


        python3 m83_reconstruction inits/m83_compact.ini


- Extended reconstruction 


        python3 m83_reconstruction inits/m83_extended.ini


- Single dish reconstruction



        python3 m83_reconstruction inits/m83_sd.ini
        
        

3)  Plotting results




Synthetic Data:
-------------------------------------------------------------------------------
A) Synthetic RESOLVE data:

    create and reconstruct data from RESOLVE prior, execute:
         mock.py 


B) Synthetic CASA data, CLEAN reconstruction:

    1) download CASA 5.6.2-2 from https://casa.nrao.edu/casa_obtaining.shtml

    2) Create data by executing:
         casa -c combined_observation.py

    3) Reconstruct with CASA by executing:
         casa -c combined_reconstruction.py


C) Synthetic CASA data, RESOLVE reconstruction:

    1) extract single dish data with:
        python3 extract_data_sd.py m51c.aca.tp.sd.ms 0 0 0 -o outdir_sd

    2) extract interferometer data with:
        python3 ms2hdf5.py combined_observation.alma.out10.ms -o outdir_12m

    3) adjust filepath of sd & interferometer datasets in inits/mock_combined.ini

    4) reconstruct with:
        pyhton3 mock_reconstruction.py inits/mock_combined.ini
