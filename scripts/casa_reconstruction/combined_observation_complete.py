# Set simobserve to default parameters
default("simobserve")
# Our project name will be m51c, and all simulation products will be placed in a subdirectory m51c/
project="m51c"

# Model sky = Halpha image of M51 
os.system('curl https://casaguides.nrao.edu/images/3/3f/M51ha.fits.txt -f -o M51ha.fits')
skymodel         =  "M51ha.fits"

# Set model image parameters:
indirection="J2000 13h37m00.75s -29d51m58.6s"

incell="0.1arcsec"
inbright="0.004"
incenter="330.076GHz"
inwidth="50MHz"

# have simobserve calculate mosaic pointing locations:
setpointings       =  True
integration        =  "10s"
mapsize            =  "1arcmin"
maptype            =  "hex"
pointingspacing    =  "9arcsec"      # this could also be specified in units of the primary beam e.g. "0.5PB"

obsmode            =  "int"
antennalist        =  "ALMA;0.5arcsec"
totaltime          =  "3600s"

graphics           =  "both"
simobserve()


integration        =  "10s"
mapsize            =  "1.3arcmin"
maptype            =  "square"

obsmode            = "sd"
sdantlist          = "aca.tp.cfg"
sdant              = 0
refdate            = "2012/12/01"
totaltime          =  "2h"

simobserve()

integration        =  "10s"
mapsize            =  "1arcmin"
maptype            =  "hex"
pointingspacing    =  "15arcsec"

obsmode            = "int"
refdate            = "2012/12/02"
antennalist        =  "aca.i.cfg"
totaltime          =  "3"

simobserve()
