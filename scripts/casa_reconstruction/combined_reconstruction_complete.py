default("simanalyze")
project            =  "m51c"
vis                =  '$project.aca.i.ms,$project.aca.tp.sd.ms'  
imsize             =  [1024,1024]
cell               =  '0.2arcsec'
analyze            =  True
showpsf            =  False
showresidual       =  False
showconvolved      =  True
simanalyze()

default("simanalyze")
project            =  "m51c"
vis                =  '$project.ALMA_0.5arcsec.ms'
imsize             =  [1024,1024]
cell               =  '0.1arcsec'
modelimage         =  "$project.aca.i.image"
analyze            =  True
showpsf            =  False
showresidual       =  False
showconvolved      =  True
simanalyze()
