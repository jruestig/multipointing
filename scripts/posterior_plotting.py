import nifty5 as ift
import pickle
import sys
import numpy as np
import multipointing as mp
from matplotlib.colors import LogNorm
import matplotlib.pyplot as plt

plt.rcParams.update({'font.size': 22})

cmapname = 'binary'
cmapname = 'cividis'
cmapname = 'jet'

postplot = mp.PosteriorPlotter()


fits_file = sys.argv[1]
cfg_file = sys.argv[2]
pckl = sys.argv[3]
cfg = mp.ConfigurationParser(cfg_file, postplt=True)

fits_file2 = sys.argv[4]
cfg_file2 = sys.argv[5]
pckl2 = sys.argv[6]
cfg2 = mp.ConfigurationParser(cfg_file2, postplt=True)


res_reconstruction = postplot.load_resolve(cfg, pckl)
casa_reconstruction, header = postplot.load_casa(cfg, fits_file, cfg2.sky['minfreq'] - cfg.sky['minfreq'])

# adjust image dimensions
bmaj = postplot.degtorad(header['BMAJ']) # Beam major-axis in rad
bmin = postplot.degtorad(header['BMIN'])  # Beam minor-axis in rad
beam = np.pi * postplot.radtoarcsec(bmaj)/2 * postplot.radtoarcsec(bmin)/2  # beam in deg
casa_reconstruction = casa_reconstruction / beam
fov = cfg.sky['fov'] * 60  # fov in arcsec
pxl_in_arcsec = (fov / max(cfg.sky["Nx"], cfg.sky["Ny"]))**2
res_reconstruction = res_reconstruction / pxl_in_arcsec / 2


res_reconstruction2 = postplot.load_resolve(cfg2, pckl2)
casa_reconstruction2, header2 = postplot.load_casa(cfg2, fits_file2)
# adjust image dimensions
bmaj = postplot.degtorad(header2['BMAJ']) # Beam major-axis in rad
bmin = postplot.degtorad(header2['BMIN'])  # Beam minor-axis in rad
beam = np.pi * postplot.radtoarcsec(bmaj)/2 * postplot.radtoarcsec(bmin)/2  # beam in deg
casa_reconstruction2 = casa_reconstruction2 / beam

fov = cfg2.sky['fov'] * 60  # fov in arcsec
pxl_in_arcsec = (fov / max(cfg2.sky["Nx"], cfg2.sky["Ny"]))**2
res_reconstruction2 = res_reconstruction2 / pxl_in_arcsec / 2


point1, point2 = np.array(([-0.0003, 0.0002], [0.00023, -0.0006]))

fig = plt.figure(figsize=(15*2, 8*2))
axes = fig.subplots(nrows=2, ncols=2)


n = 0
for ii, rec in enumerate([casa_reconstruction, res_reconstruction, casa_reconstruction2, res_reconstruction2]):
    if ii > 1:
        n = 1
        ii -= 2
    maximum = 0.5
    xmin, xmax, ymin, ymax = postplot.get_rectangular(rec.domain, point1, point2)
    print(xmin, xmax, ymin, ymax)
    Fr = mp.FieldReducer(rec.domain, xmin, xmax, ymin, ymax)
    ds1, ds2 = rec.domain[0].distances
    # maximum = Fr(rec).to_global_data().max()
    axes[ii, n].imshow(Fr(rec).to_global_data(),
            # extent=[ymin*ds1, ymax*ds1, xmin*ds2, xmax*ds2], 
            extent =[0, (ymax-ymin)*postplot.radtoarcsec(ds1), 
                (xmax-xmin)*postplot.radtoarcsec(ds2), 0],
            cmap=plt.get_cmap(cmapname),
            #cmap=mp.colormaps.test.violet_cm,
            clim=(0.005, maximum)
            )
    from mpl_toolkits.axes_grid1 import make_axes_locatable
    divider = make_axes_locatable(axes[ii, n])
    cax = divider.append_axes('right', size='5%', pad=0.05)
    cb = plt.colorbar(axes[ii, 0].get_images()[0], cax=cax)
    pixsize = (postplot.radtoarcsec(ds1) * postplot.radtoarcsec(ds2))
    cb.set_label(r"Jy/(arcsec^2)")

axes[1, 0].set_xlabel('arcsec')
axes[1, 1].set_xlabel('arcsec')
axes[0, 0].set_ylabel('arcsec')
axes[1, 0].set_ylabel('arcsec')

# plt.show()
plt.savefig(fname='./m83_reconstruction.png')
