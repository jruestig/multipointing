import nifty5 as ift
import numpy as np
import multipointing as mp
import h5py
from scipy.constants import c
from os.path import join
import sys

np.random.seed(42)

cfg_file = sys.argv[1]
cfg = mp.ConfigurationParser(cfg_file)

Nx, Ny = cfg.sky['Nx'], cfg.sky['Ny']
fov = cfg.sky['fov']
maxfreq, minfreq = cfg.sky['maxfreq'], cfg.sky['minfreq']
print('maxfreq: ', maxfreq, 'minfreq: ', minfreq)
phscntr = cfg.sky['phscntr']
rotang = cfg.sky['rotation']
units = cfg.sky['units']

s_space = ift.RGSpace((Nx, Ny), distances=(fov/max([Nx, Ny]),)*2)
h_space = s_space.get_default_codomain()
ht = ift.HarmonicTransformOperator(h_space, s_space)
power_space = ift.PowerSpace(h_space)

sky_amp_conf = cfg.prior
sky_amp_conf['keys'] = ['tau', 'phi']
sky_amp_conf['target'] = power_space

sky_amp = ift.SLAmplitude(**sky_amp_conf)
A = ift.PowerDistributor(h_space, sky_amp.target[0]) @ sky_amp
corr = ift.CorrelatedField(s_space, sky_amp)
diffuse = (corr.clip(-200, 200)).exp()

priorp = mp.PriorPlotter(cfg.output, diffuse, units=units)
priorp.plot_prior(10)


RNd = []
# Defining the single dish response
if cfg.mode != "inter":
    sd_base = cfg.sd['sd_base']
    sd_names = cfg.sd['sd_names']
    sd_dish = cfg.sd['dish']

    sd_data = np.load(join(sd_base, sd_names[0]))
    sd_band = sd_data["band"]
    mean_lambda = c/(sd_band[0]).mean()

    sd_dish = sd_dish / mean_lambda
    sd_d = np.load(join(sd_base, sd_names[0]))['data'][:, 0, :]
    sd_var = np.load(join(sd_base, sd_names[0]))['var']
    sd_pointings = np.load(join(sd_base, sd_names[0]))['pointing']
    
    sd_pointings_rela = mp.relative_to_image_center(sd_pointings, phscntr)
    sd_pointings_rela = mp.rotation(rotang, sd_pointings_rela)
    sd_pixpos = mp.get_pixel(diffuse.target[0], sd_pointings_rela, units=units)

    pb_sd = mp.get_primary_beam(s_space, np.array([0., 0.]), sd_dish,
                                zero_pad=True, units=units)
    Rsd = mp.SingleDish(diffuse.target[0], sd_pixpos, pb_sd)

    sd_d = 0.5*(sd_d[:, 0].real + sd_d[:, 1].real)
    sd_d = ift.from_global_data(Rsd.target, sd_d)
    sd_var = ift.from_global_data(Rsd.target, sd_var)

    Nsd = ift.DiagonalOperator(sd_var)
    RNd.append([Rsd, Nsd, sd_d])

    print(mean_lambda)

if cfg.mode != "sd":
    for interferometer in cfg.inter:
        inter_base = interferometer['inter_base']
        field_ids = interferometer['field_ids']
        inter_dish = interferometer['dish']
        band_id = interferometer['band_id']
        pb_factor = interferometer['pb_factor']

        # Band choice
        header = {}
        hname = inter_base + 'header.hdf5'
        with h5py.File(hname, 'r') as f:
            for kk, vv in f.items():
                header[kk] = vv[:]
        in_band = header["freqs"][band_id]
        in_band_select = (in_band < maxfreq) & (in_band > minfreq)
        # print(in_band[in_band_select])
        mean_lambda = c/(in_band[in_band_select]).mean()
        inter_dish = inter_dish/mean_lambda

        psize = mp.patch_size(s_space, inter_dish, factor=pb_factor, units=units)
        if interferometer['psize'] is not None:
            psize = interferometer['psize']
        print('Patch Size: ', psize)

        inter_relative = mp.relative_to_image_center(
            header['field_directions'], phscntr).reshape(-1, 2)
        # Rotation of sky
        inter_relative = mp.rotation(rotang, inter_relative)

        xmin, xmax, ymin, ymax = mp.get_pixel_patch(
            diffuse.target[0], inter_relative[np.random.choice(field_ids)],
            (psize, psize), units=units)
        Fr = mp.FieldReducer(diffuse.target, xmin, xmax, ymin, ymax)
        pb = mp.get_primary_beam(Fr.target[0], [0., 0.], inter_dish,
                                 units=units)
        pbop = ift.DiagonalOperator(pb)

        for field_id in field_ids:
            xmin, xmax, ymin, ymax = mp.get_pixel_patch(
                diffuse.target[0],
                inter_relative[field_id],
                (psize, psize),
                units=units)
            Fr = mp.FieldReducer(diffuse.target, xmin, xmax, ymin, ymax)

            fname = inter_base + "field_{}_v2.hdf5".format(field_id)
            d = {}
            with h5py.File(fname, 'r') as f:
                for kk, vv in f.items():
                    d[kk] = vv[:]
            uvw = d["uvw"]
            uv = uvw[..., 0:2]
            del(uvw)
            uv *= -1
            # Rotation of sky, rotates Fourier domain accordingly
            uv = mp.rotation(rotang, uv)
            uv = np.broadcast_to(
                uv, in_band[in_band_select].shape + uv.shape)
            uv = np.transpose(uv, (1, 2, 0)) / (c/in_band[in_band_select])
            uv = np.transpose(uv, (0, 2, 1)) * units

            vis = d["vis"]
            vis = vis[..., 0] + vis[..., 1]
            vis = 0.5*vis[..., in_band_select]

            var = d["var"]
            var = var[..., 0] + var[..., 1]
            var = np.repeat(
                var.reshape(-1, 1), len(in_band[in_band_select]), axis=1)

            uv[np.abs(uv) > .5/s_space.distances[0]] = np.nan
            too_big = ~np.any(np.any(np.isnan(uv), axis=1), axis=1)
            print("clipped {} of {} baselines".format(np.sum(~too_big), len(too_big)))

            uv = uv[too_big].reshape(-1, 2)
            vis = vis[too_big].reshape(-1)
            var = var[too_big].reshape(-1)

            inter = mp.Interferometer(uv, Fr.target)
            vis = ift.from_global_data(inter.target, vis)
            var = ift.from_global_data(inter.target, var)

            Rinter = inter @ pbop @ Fr
            Ninter = ift.DiagonalOperator(var)
            RNd.append([Rinter, Ninter, vis])




like_list = [ift.GaussianEnergy(d, N) @ R for R, N, d in RNd]
likelihood = mp.EnergySum(like_list)
likelihood = likelihood @ diffuse

tolerance = cfg.minimization['tolerance']
maxsteps = cfg.minimization['maxsteps']
cg_sampling = cfg.minimization['cg_sampling']
N_iterations = cfg.minimization['N_iterations']
Nsamples = cfg.minimization['Nsamples']

ic_newton = ift.GradInfNormController(name='Newton', tol=tolerance,
                                      iteration_limit=maxsteps)
minimizer = ift.NewtonCG(ic_newton)
ic_sampling = ift.GradientNormController(iteration_limit=cg_sampling)
# ic_sampling = ift.AbsDeltaEnergyController(0.5, iteration_limit=cg_sampling)
H_op = ift.StandardHamiltonian(likelihood, ic_sampling)

plotter = mp.Plotter(cfg.output, diffuse, sky_amp, RNd, units=units)
plotter.information(cfg.mode)

print(cfg.output)
mean = ift.full(H_op.domain, 0.)
for i in range(N_iterations):
    # Draw new samples and minimize KL
    print("Global Iteration: {}".format(i))
    KL = ift.MetricGaussianKL(mean, H_op, Nsamples, mirror_samples=True)
    KL, convergence = minimizer(KL)
    mean = KL.position
    ift.do_adjust_variances(mean, A, minimizer)
    plotter.plot(KL, suffix="analysis_kl_{}".format(i))
    plotter.plot_fits(KL, suffix="analysis_kl_{}".format(i))
    plotter.save_pickle(mean, file_name="pickle_kl_{}".format(i))
for i, sample in enumerate(KL.samples):
    plotter.save_pickle(sample, file_name="sample_{}".format(i))
