# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2019 Max-Planck-Society
# Author: Philipp Arras, Julian Ruestig

import sys
from os.path import exists, isdir, join, split
from os import makedirs
import argparse

import h5py
import numpy as np
from casacore.tables import table


def _liststr2ascii(l):
    return [ss.encode('ascii', 'ignore') for ss in l]


def load_from_ms(ms, dcol):
    if not isdir(ms):
        raise RuntimeError('Input not found: {}'.format(ms))
    # FIXME Check data types, save only single precision if possible

    print('Load data.')
    t = table(ms, readonly=True)
    d = {
        'uvw': t.getcol("UVW"),  # [:, uvw]
        'vis': np.array(t.getcol(dcol), dtype=np.complex128),  # [:, ch, corr]
        # FIXME Support WEIGHT_SPECTRUM eventually
        'var': (t.getcol("SIGMA")**2),  # [:, corr]
        'flag': np.array(t.getcol('FLAG'), np.bool),  # [:, ch, corr]
        'data_desc_id': t.getcol('DATA_DESC_ID'),  # [:]
        'field': t.getcol('FIELD_ID'),  # [:]
        'ant1': t.getcol('ANTENNA1'),  # [:]
        'ant2': t.getcol('ANTENNA2'),  # [:]
        'time': t.getcol('TIME') # [:]
    }
    t.close()

    print('# Rows: {}'.format(d['vis'].shape[0]))
    print('# Channels: {}'.format(d['vis'].shape[1]))
    print('# Correlations: {}'.format(d['vis'].shape[2]))
    print('# Spectral Windows: {}'.format(len(set(d['data_desc_id']))))

    t = table(join(ms, 'SPECTRAL_WINDOW'), readonly=True)
    d['freqs'] = t.getcol('CHAN_FREQ')
    t.close()

    t = table(join(ms, 'SOURCE'), readonly=True)
    d['directions'] = t.getcol('DIRECTION')
    d['sourcenames'] = _liststr2ascii(t.getcol('NAME'))
    t.close()

    t = table(join(ms, 'FIELD'), readonly=True)
    d['field_directions'] = t.getcol('DELAY_DIR')
    t.close()

    t = table(join(ms, 'OBSERVATION'), readonly=True)
    d['trange'] = t.getcol('TIME_RANGE')[0]
    d['telescope'] = _liststr2ascii(t.getcol('TELESCOPE_NAME'))
    d['observer'] = _liststr2ascii(t.getcol('OBSERVER'))
    t.close()

    return d


def apply_flagging_and_spw(d, spw=0):
    def _apply_mask(mask, dct):
        return {key: arr[mask] for key, arr in dct.items()}

    new_d = {
        'uvw': d["uvw"],
        'vis': d["vis"],
        'var': d["var"],
        'flag': d["flag"],
        'data_desc_id': d["data_desc_id"],
        'field': d["field"]
    }

    mask_spw = new_d["data_desc_id"] == spw
    new_d = _apply_mask(mask_spw, new_d)

    mask_flag = np.all(~new_d["flag"], axis=2)
    vis = new_d["vis"]
    vis[~mask_flag] = np.nan
    print('  {}/{} single visibilities are flagged.'.format(
          np.sum(np.isnan(vis)), vis.size))
    all_flagged = np.all(np.all(np.isnan(vis), axis=1), axis=1)
    new_d = _apply_mask(~all_flagged, new_d)
    return new_d


def get_field(d, field_id):
    mask = d["field"] == field_id
    new_d = {
        'uvw': d["uvw"][mask],
        'vis': d["vis"][mask],
        'var': d["var"][mask],
    }
    print("uvw shape: ", new_d["uvw"].shape)
    return new_d


def write_to_hdf5(fname, d):
    with h5py.File(fname, 'w') as f:
        for kk, vv in d.items():
            f.create_dataset(kk, data=vv)
    print('Wrote {}'.format(fname))



description = 'Extracts interferometer data from a measurement set.'
parser = argparse.ArgumentParser(prog='extract_data', description=description)
parser.add_argument('input_dir', help='path to measurement set')
parser.add_argument(
    "-o",
    "--output-dir",
    default='.',
    help="output directory, default: .",
    metavar='<dir>')

args = parser.parse_args()
outdir = args.output_dir
if not exists(outdir):
    makedirs(outdir)


if __name__ == '__main__':
    ms = args.input_dir
    if not exists(ms):
        raise RuntimeError('Input measurement set not found.')
    print('measurement set name: ', ms.split('/')[-2])
    fname = ms.split('/')[-2]
    dcol = "DATA"
    d = load_from_ms(ms, dcol)

    header_data = {}
    header_data['freqs'] = d['freqs']
    header_data['field_directions'] = d['field_directions']
    header_data['trange'] = d['trange']
    header_data['telescope'] = d['telescope']
    header_data['observer'] = d['observer']
    write_to_hdf5(
            join(outdir, '{}_{}_header.hdf5'.format(fname, dcol)),
            header_data)

    d = apply_flagging_and_spw(d)

    min_id, max_id = d['field'].min(), d['field'].max()
    for ii in range(max_id + 1):
        field_id = ii + min_id
        tmp_d = get_field(d, field_id)
        write_to_hdf5(
                join(outdir,'{}_{}_field_{}_v2.hdf5'.format(fname, dcol, field_id)), 
                tmp_d)
