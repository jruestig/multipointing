import nifty5 as ift
import numpy as np
import multipointing as mp

from scipy.constants import c
from os.path import join
import sys

np.random.seed(34)
np.seterr(all="raise")


mode = int(sys.argv[1])
if mode is 0:
    mode = "combined"
elif mode is 1:
    mode = "inter"
elif mode is 2:
    mode = "sd"

spacing_factor = 2

output = "mockdata/{}_ss".format(mode)

sd_data = np.load("/home/j/pro/master/data/totalpower/" +
                  "uid___A002_Xb12f3b_Xbb8b.PM01_sw17.npz")
inter_data = np.load(join(
    "/home/j/pro/master/radiodaten/m83/mockobs/observation/",
    "alma_obs/outdir/casa_m83_mockobs_alma.alma.out10_field0_spw0.npz"))


Npix = 128
fov = 2.0  # * (np.pi / (60*180))
phscntr = np.array([-2.71829463, -0.52126411])
D = 10.7
smalld = 0.75
median_lambda = c/110e9
# median_lambda = c/220e9
units = (np.pi / (60*180))

s_space = ift.RGSpace((Npix,)*2, distances=fov/Npix)
h_space = s_space.get_default_codomain()
ht = ift.HarmonicTransformOperator(h_space, s_space)
power_space = ift.PowerSpace(h_space)
sky_amp_conf = {
    'n_pix': 128,  # 64 spectral bins
    # Spectral smoothness (affects Gaussian process part)
    'a': 2,  # relatively high variance of spectral curvature
    'k0': 1.7,  # quefrency mode below which cepstrum flattens
    # Power-law part of spectrum:
    'sm': -3.7,  # preferred power-law slope
    'sv': 0.5,  # low variance of power-law slope
    # 'im': 7.9,  # y-intercept mean, in-/decrease for more/less contrast
    'im': -3.5,
    'iv': .4,   # y-intercept variance
    'target': power_space
}
# zero_mode = {
#     "za": 2.5,
#     "zq": 1.e-7,
#     "keys": ["xi", "zero_mode"]
# }
sky_amp = ift.SLAmplitude(**sky_amp_conf)
corr = ift.CorrelatedField(s_space, sky_amp)  # , a=3.5, q=1e-6)
diffuse = (corr.clip(-200, 200)).exp()
sky_space = diffuse.target

# Single dish response
px = Npix // 8
sd_pixpos = np.array([
     # [px*2, px*2], [px*2, px*3], [px*2, px*4], [px*2, px*5], [px*2, px*6],
     # [px*4, px*2], [px*4, px*3], [px*4, px*4], [px*4, px*5], [px*4, px*6],
     # [px*6, px*2], [px*6, px*3], [px*6, px*4], [px*6, px*5], [px*6, px*6]
     [px*4, px*4],
     ],
    dtype=np.int)
sd_coverage = mp.field_coverage(s_space, sd_pixpos)
dish = np.array([D / median_lambda, smalld / median_lambda]).T
pb_sd = mp.get_primary_beam(s_space, np.array([0., 0.]), dish, zero_pad=True,
                            units=units)
Rsd = mp.SingleDish(sky_space[0], sd_pixpos, pb_sd)

# Noise covariance
sd_var = ift.full(Rsd.target, .01)
Nsd = ift.DiagonalOperator(sd_var)

# # Mock data
# for ii in range(10):
#     mockpos = ift.from_random("normal", diffuse.domain)
#     sky_mock = diffuse(mockpos)
#     print(sky_mock.mean(), sky_mock.var())
#     p = ift.Plot()
#     p.add(sky_mock)
#     p.output()

mockpos = ift.from_random("normal", diffuse.domain)
sky_mock = diffuse(mockpos)

sd_data = Rsd(sky_mock) + Nsd.draw_sample()


# Interferometer
pointings = np.array([
    # [0, +0.0001],  # [-0.0001, 0], [+0.0001, 0],
    # [0, -0.0001],  # [-0.00005, -0.0001],
    # [-0.0001, -0.0001],  [+0.0001, -0.0001],  # [0.00005, -0.0001],
    [0, 0]  # , [+0.0001, -0.0001],  # [-0.0001, 0.0001]
])
inter_coverage = mp.field_coverage(s_space,
                                   mp.get_pixel(s_space, pointings/units),
                                   counts=False)

# uv handling
uv = inter_data["uv"].reshape(-1, 2)
if spacing_factor is not None:
    r = np.sqrt(uv.T[0]**2 + uv.T[1]**2)
    short_spacing_cut = r > D*spacing_factor
    uv = uv[short_spacing_cut]

uv = uv/median_lambda * units
uv[np.abs(uv) > .5/s_space.distances[0]] = np.nan
mask = ~(np.any(np.isnan(uv), axis=1))
unflagged = np.sum(mask)
print("removed {} rows ({}%)".format(
    mask.size - unflagged, (1 - unflagged/mask.size)*100))
uv = uv[mask]

d_dom = ift.DomainTuple.make(
    (ift.UnstructuredDomain(uv.shape[0]),
     ift.UnstructuredDomain(uv.shape[1])))

like_list = []
RNd = []
RNd.append([Rsd, Nsd, sd_data])
inter_info_source = ift.full(s_space, 0.)
for pointing in pointings:
    inter = mp.Interferometer(uv, ift.DomainTuple.make(s_space))
    pb_op = ift.DiagonalOperator(
        mp.get_primary_beam(s_space, pointing/units, dish, units=units))
    phs = mp.PhaseShift(-uv, pointing)
    Rinter = phs @ inter @ pb_op
    Ninter = ift.DiagonalOperator(ift.full(Rinter.target, 0.05))
    d_inter = Rinter(sky_mock) + Ninter.draw_sample()
    inter_info_source = inter_info_source + Rinter.adjoint(
        Ninter.adjoint(d_inter))
    like_list.append(
        ift.GaussianEnergy(mean=d_inter, inverse_covariance=Ninter) @ Rinter)
    RNd.append([Rinter, Ninter, d_inter])


print(mode)
like_sd = ift.GaussianEnergy(mean=sd_data, inverse_covariance=Nsd) @ Rsd
if mode is "combined":
    like_list.append(like_sd)
    likelihood = mp.EnergySum(like_list)
elif mode is "sd":
    likelihood = like_sd
    RNd = [RNd[0]]
elif mode is "inter":
    likelihood = mp.EnergySum(like_list)
    RNd = RNd[0:]
likelihood = likelihood @ diffuse

# Reconstruction
plotter = mp.Plotter(output, diffuse, sky_amp, dish, RNd, units=60,
                     mocksky=sky_mock)
plotter.set_up(mp.get_primary_beam(s_space, [0., 0.], dish, units=units),
               sd_coverage,
               Rsd.adjoint(Nsd.adjoint(sd_data)),
               mp.field_coverage(h_space, uv, physical=True),
               inter_coverage,
               inter_info_source)


maxsteps = 5
tolerance = 1e-8
Nsamples = 5
cg_sampling = 20
N_iterations = 5
print(maxsteps, tolerance, Nsamples, cg_sampling, N_iterations)

ic_newton = ift.GradInfNormController(name='Newton', tol=tolerance,
                                      iteration_limit=maxsteps)
minimizer = ift.NewtonCG(ic_newton)
ic_sampling = ift.GradientNormController(iteration_limit=cg_sampling)
H_op = ift.StandardHamiltonian(likelihood, ic_sampling)


mean = ift.full(H_op.domain, 0.)
H = ift.EnergyAdapter(mean, H_op, want_metric=True)
# H, convergence = minimizer(H)
mean = H.position
for i in range(N_iterations):
    # Draw new samples and minimize KL
    KL = ift.MetricGaussianKL(mean, H_op, Nsamples, mirror_samples=True)
    KL, convergence = minimizer(KL)
    mean = KL.position
    plotter.plot(KL, mockpos=mockpos, pspec_limits=(1e-11, 1e1),
                 suffix="{}_kl_{}".format(mode, i))
    # plotter.save_pickle(mean, "kl_{}".format(i))
KL = ift.MetricGaussianKL(mean, H_op, Nsamples*2, mirror_samples=True)
plotter.plot(KL, mockpos=mockpos, pspec_limits=(1e-11, 1e1),
             suffix='posterior_analysis')
