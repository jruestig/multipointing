import nifty5 as ift
import pickle
import sys
import numpy as np
import multipointing as mp
from matplotlib.colors import LogNorm
from astropy.io import fits
from scipy import ndimage
import matplotlib.pyplot as plt

plt.rcParams.update({'font.size': 22})


casa_rec_fits = sys.argv[1]
truth_fits = sys.argv[2]

cfg_file = sys.argv[3]
pckl = sys.argv[4]
base = "/".join(cfg_file.split("/")[:-1])
nsamples = sys.argv[5]

postplot = mp.PosteriorPlotter()

cfg = mp.ConfigurationParser(cfg_file, postplt=True)
diffuse = postplot.load_diffuse(cfg)

position = pickle.load(open(pckl, 'rb'))
sc = ift.StatCalculator()
for ii in range(int(nsamples)):
    sample = pickle.load(open(base + "/sample_{}.pickle".format(ii), "rb"))
    samplesky = diffuse.force(sample.unite(position))
    sc.add(samplesky)
res_reconstruction = sc.mean
res_var = sc.var

pxl_over_arcsec = postplot.radtoarcsec(postplot.arcmintorad(sc.mean.domain[0].distances[0]))**2


res_reconstruction = ndimage.rotate(res_reconstruction.to_global_data(), -90, reshape=True)
res_reconstruction = ndimage.rotate(res_reconstruction, -np.degrees(cfg.sky['rotation']), reshape=False)
res_var = ndimage.rotate(res_var.to_global_data(), -90, reshape=True)
res_var = ndimage.rotate(res_var, -np.degrees(cfg.sky['rotation']), reshape=False)
res_domain = ift.RGSpace(np.flip(np.array(diffuse.target[0].shape)), postplot.arcmintorad(np.flip(np.array(diffuse.target[0].distances))))
res_reconstruction /= pxl_over_arcsec


# CASA Reconstruction
data, header = fits.getdata(casa_rec_fits, header=True)
crval3 = header['CRVAL3']
cdelt3 = header['CDELT3']
casa_reconstruction = data[0][0]
casa_reconstruction[np.isnan(casa_reconstruction)] = 0
nax1, delt1 = header['NAXIS1'], postplot.degtorad(-header['CDELT1'])
nax2, delt2 = header['NAXIS2'], postplot.degtorad(header['CDELT2'])
casa_domain = ift.RGSpace([nax1, nax2], distances=(delt1, delt2))
casa_reconstruction = ift.from_global_data(casa_domain, casa_reconstruction)

bmaj = postplot.degtorad(header['BMAJ']) # Beam major-axis in rad
bmin = postplot.degtorad(header['BMIN'])  # Beam minor-axis in rad
beam = np.pi * postplot.radtoarcsec(bmaj)/2 * postplot.radtoarcsec(bmin)/2  # beam in degree

casa_reconstruction = casa_reconstruction / beam


# ground truth
tdata, thead = fits.getdata(truth_fits, header=True)
crval3 = thead['CRVAL3']
cdelt3 = thead['CDELT3']
nax1, delt1 = thead['NAXIS1'], postplot.degtorad(-thead['CDELT1'])
nax2, delt2 = thead['NAXIS2'], postplot.degtorad(thead['CDELT2'])
center = np.array([postplot.degtorad(i) for i in [thead['CRVAL1']-360, thead['CRVAL2']]])
casa_domain = ift.RGSpace([nax1, nax2], distances=(delt1, delt2))
truth = ift.from_global_data(casa_domain, tdata[0])
truth = truth / pxl_over_arcsec


# Resolve reconstruction
shift_center = cfg.sky['phscntr'] - center
shift_center = np.array([0, 0])
shift = np.array([shift_center[0]/delt1, shift_center[1]/delt2])
res_reconstruction = ndimage.shift(res_reconstruction, -shift)
res_var = ndimage.shift(res_var, -shift)
res_reconstruction = ift.from_global_data(res_domain, res_reconstruction)
res_var  = ift.from_global_data(res_domain, res_var)


fidelity_casa = casa_reconstruction/ift.absolute(casa_reconstruction - truth)
fidelity_casa = (casa_reconstruction - truth)**2

rms_casa = np.nansum((((casa_reconstruction-truth)**2).val))
truth = ift.from_global_data(res_domain, tdata[0]/pxl_over_arcsec)
fidelity_res = res_reconstruction/ift.absolute(res_reconstruction - truth)
fidelity_res = (res_reconstruction - truth)**2 



rms_res = np.nansum((((res_reconstruction-truth)**2).val))
print('Rootmeansquare casa: ', rms_casa)
print('Rootmeansquare res: ', rms_res)

print("totflx truth: ", np.nansum(truth.val)*0.4)
print("totflx casa: ", np.nansum(casa_reconstruction.val)*0.4)
print("totflx res: ", np.nansum(res_reconstruction.val)*0.4)

point1, point2 = np.array(([-0.0004, -0.0004], [0.0004, 0.0004]))
maximum = 0.4
cmapname = 'viridis'
cmapname = 'cividis'
cmapname = 'jet'

fig = plt.figure()
fig.set_size_inches(27, 15)
axes = fig.subplots(nrows=2, ncols=3)
r = 0
for ii, rec in enumerate([truth, casa_reconstruction, fidelity_casa, truth, res_reconstruction, fidelity_res]):
    if ii > 2:
        r = 1
        ii -= 3
    print(ii)
    xmin, xmax, ymin, ymax = postplot.get_rectangular(rec.domain, point1, point2)
    # print(xmin, xmax, ymin, ymax)
    Fr = mp.FieldReducer(rec.domain, xmin, xmax, ymin, ymax)
    ds1, ds2 = rec.domain[0].distances
    #maximum = Fr(rec).to_global_data().max()
    if ii != 2:
        axes[r, ii].imshow(Fr(rec).to_global_data(),
                # extent=[ymin*ds1, ymax*ds1, xmin*ds2, xmax*ds2], 
                extent =[0, (ymax-ymin)*postplot.radtoarcsec(ds1), 
                    (xmax-xmin)*postplot.radtoarcsec(ds2), 0],
                cmap=plt.get_cmap(cmapname),
                # cmap=mp.colormaps.test.violet_cm, 
                clim=(0., maximum)
                )
    else:
        axes[r, ii].imshow(Fr(rec).to_global_data(),
                extent =[0, (ymax-ymin)*postplot.radtoarcsec(ds1), 
                    (xmax-xmin)*postplot.radtoarcsec(ds2), 0],
                cmap=plt.get_cmap(cmapname),
                clim=(0., .02)
                )
    from mpl_toolkits.axes_grid1 import make_axes_locatable
    divider = make_axes_locatable(axes[r, ii])
    cax = divider.append_axes('right', size='5%', pad=0.05)
    cb = plt.colorbar(axes[r, ii].get_images()[0], cax=cax)
    pixsize = (postplot.radtoarcsec(ds1) * postplot.radtoarcsec(ds2))
    # cb.set_label(r"\mathrm{Jy}/{} \mathrm{arcsec}^2".format(pixsize))
    axes[r, 0].set_ylabel('arcsec')
    axes[1, ii].set_xlabel('arcsec')

    if ii != 2:
        cb.set_label(r"Jy/arcsec^2")
    else:
        cb.set_label("RMS".format(pixsize))



plt.savefig(fname='./synth.png')
