import multipointing as mp
import numpy as np
import nifty5 as ift
import sys
import h5py
from os.path import exists


phscntr = np.array([-2.71829463, -0.52126411])
Npix = 512
fov = 6.5*(np.pi/(60*180))


hname = sys.argv[1]
if not exists(hname):
    raise RuntimeError('Headerdata not found')

header = {}
with h5py.File(hname, 'r') as f:
    for kk, vv in f.items():
        header[kk] = vv[:]

directions = header["field_directions"].reshape(-1, 2)

sky = ift.RGSpace((Npix,)*2, distances=(fov/Npix,)*2)

reldir = mp.relative_to_image_center(directions, phscntr)
pixpos = mp.get_pixel(sky, reldir)
onsky = mp.field_coverage(sky, pixpos)

field_ids = [4, 21, 22, 32, 33, 43, 44]
for field_id in field_ids:
    pixpos = mp.get_pixel(sky, reldir[field_id])
    one_point = mp.field_coverage(sky, pixpos)
    onsky = onsky + one_point

p = ift.Plot()
p.add(onsky)
p.output()
