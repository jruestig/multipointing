import argparse
from os import makedirs
from os.path import basename, exists, isdir, join

import numpy as np

from casacore.tables import table, taql


def extract_data_sd():
    description = 'Extracts single dish data from a measurement set.'
    parser = argparse.ArgumentParser(
        prog='extract_data', description=description)

    parser.add_argument('input_dir', help='path to measurement set')
    parser.add_argument('spectral_window',
                        help="spectral window to be considered")
    parser.add_argument(
        'pol_first', help='number of first polarisation column', type=int)
    parser.add_argument(
        'pol_second', help='number of second polarisation column', type=int)

    parser.add_argument(
        "-o",
        "--output-dir",
        default='.',
        help="output directory, default: .",
        metavar='<dir>')

    args = parser.parse_args()
    pol_fst = args.pol_first
    pol_snd = args.pol_second
    sw_number = float(args.spectral_window)

    ms = args.input_dir
    print('measurement set name: ', ms.split('/')[-2])
    ms_name = ms.split('/')[-2]
    if not isdir(ms):
        raise RuntimeError('Input not found: {}'.format(ms))

    outdir = args.output_dir
    if not exists(outdir):
        makedirs(outdir)

    # Import Single dish measurement, FLOAT_DATA in ALMA.ms, DATA in Casa.ms
    t = table(ms, readonly=True)
    t1 = taql("select DATA from $t where DATA_DESC_ID=$sw_number")
    sd_data = t1.getcol("DATA")
    # t1 = taql("select FLOAT_DATA from $t where DATA_DESC_ID=$sw_number")
    # sd_data = t1.getcol("FLOAT_DATA")
    t2 = taql("select SIGMA from $t where DATA_DESC_ID=$sw_number")
    sd_sigma = t2.getcol("SIGMA")
    sw = t.getcol("DATA_DESC_ID")
    t.close()

    tsw = table(join(ms, "SPECTRAL_WINDOW"))
    band = tsw.getcol("CHAN_FREQ")
    tsw.close()

    sd_var = sd_sigma[:, pol_fst]**2 + sd_sigma[:, pol_snd]**2
    sw_mask = sw == sw_number

    tpointing = table(ms + "/POINTING", readonly=True)
    sd_pointing = tpointing.getcol("DIRECTION")
    tpointing.close()

    sd_pointing = sd_pointing[sw_mask]

    print("Shape: sd_data {}, sd_var {}, sd_pointing {}.".
          format(sd_data.shape, sd_var.shape, sd_pointing.shape))

    npys = [sd_data, sd_var, sd_pointing, band]
    names = ['data', 'var', 'pointing', "band"]
    np.savez_compressed(
        join(outdir, ms_name + '.npz'), **dict(zip(names, npys)))


if __name__ == '__main__':
    extract_data_sd()
